function uploadFile() {
    var form = document.forms.forma,
      formData = new FormData(form),
      xhr = new XMLHttpRequest(),
      result = document.getElementById('result'),
      submit = document.getElementById('submit'),
      progress_line = document.createElement('DIV');

    submit.style.display = 'none';
    progress_line.id = 'progress_line';
    progress_line.classList.add('progress_line');
    document.body.insertBefore(progress_line, document.body.firstChild);

    xhr.upload.addEventListener("progress", function(e) {
      if (e.lengthComputable) {
        progress_line.style.width = ((e.loaded * 100) / e.total) + '%';
      }
    }, false);

    xhr.onreadystatechange = function () {
      if (this.readyState == 4) {
        if(this.status == 200) {
          result.innerHTML = 'Код вашей комнаты - ' + xhr.responseText;
          result.innerHTML += '<br>Ссылки:<br>';
          result.innerHTML += '<a href="/tap/viewer/?whoru=host&room_id=' +
                              xhr.responseText + '" target="_blank">Для ведущего устройства</a>';
          result.innerHTML += '<br>';
          result.innerHTML += '<a href="/tap/viewer/whoru=guest&room_id=' +
                              xhr.responseText + '" target="_blank">Для ведомых</a>';
        } else {
          result.innerHTML = 'Обшибка';
        }
      }
    };
    xhr.open("POST", '/tap/uploader');

    xhr.send(formData);
    result.style.display = 'block';
}
