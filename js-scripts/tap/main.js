var presentashka, my_sock, my_tmr;
window.onload = function(){
  slider_settings = 'a(0)t(0)zw(870)h(546)scn(3000)';
  presentashka = new slider(document.getElementById("slider1"),
                       null,
                       slider_settings).show();
  presentashka.onSlideChanged = function(n) {
    if (my_sock.readyState == 1) {
      send_slide(n);
    }
    // console.log(n);
  };
  init_sock();
};

function init_sock() {
  my_sock = new WebSocket('ws://marinkevich.ru:51117/');
  my_sock.onmessage = function(e) {
    say(e.data);
  };
  my_sock.onopen = function() {
    say('opened');
    timer_start();
    if ($_GET('room_id'))
      if ($_GET('whoru') == 'host') {
        mkme_host($_GET('room_id'));
      } else if ($_GET('whoru') == 'guest') {
        mkme_guest($_GET('room_id'));
      }
  };
  my_sock.onclose = function(e) {
    if (!e.wasClean) {
      console.log('Вебсоке: ошибка: ' + e.code);
      setTimeout(init_sock, 700);
    }
  };
}

function timer_stop() {
  clearInterval(my_tmr);
}

function timer_start() {
  my_tmr = setInterval(hold_connect, 1500);
}

function hold_connect() {
    my_sock.send('drs');
}

function bb() {
  timer_stop();
  my_sock.onclose = null;
  my_sock.send('bby');
  my_sock.close();
}

function mkme_host(room_id) {
  room_id = room_id || document.getElementById('room_id').value;
  timer_stop();
  my_sock.send('hom'+room_id);
  timer_start();
  my_sock.send('sld' + presentashka.n_cur);
}

function mkme_guest(room_id) {
  timer_stop();
  room_id = room_id || document.getElementById('room_id').value;
  my_sock.onmessage = function(e) {
    say(e.data);
    var n = e.data.substring(3);
    presentashka.goto(n);
  };
  my_sock.send('gom'+room_id);
  timer_start();
}

function send_slide(n) {
  timer_stop();
  my_sock.send('sld'+n);
  timer_start();
}

function say(str4ka) {
  document.getElementById("response").innerHTML = str4ka;
}

function $_GET(key) { // Получить get-параметр из url
    var s = window.location.search;
    s = s.match(new RegExp(key + '=([^&=]+)'));
    return s ? s[1] : false;
    // Спасибу Ruslan_xDD с javascript.ru
}
