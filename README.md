# Сервис для просмотра/показа презентаций.

Основан на технологиях WebSocket и стандартных web-технологиях.

Необходима библиотека слайдера, доступная по адресу: [js-photo-slider](https://github.com/GRayHook/js-photo-slider)

Структура каталогов совпадает. Необходимые файлы:

+ /js-scripts/libs/slider.js
+ /js-scripts/libs/touches.js
+ /style/slider.css

Так же нужно обратить внимание на /client/web-morda/php-scripts/connect.php, где должен создаваться объект подключения к MySQL с именем $mysqli

Структура каталогов данного репозитория:

+ /client/web-morda/ <-- Клиентское приложение для просмотра презентаций
    + upload.php <-- Страница загрузки
    + viewer.php <-- Страница просмотра
    + js-scripts/main.js <-- Самое "вкусное". Взаимодействие с python-сервером


+ /server/python/ <-- Серверное приложение для общения устройств
    + RenameFiles.py <-- Скрипт для правильной сортировки экспортированных слайдов
    + TapServer.py <-- Логика сервера
    + WebSockets.py <-- Модуль для WebSocket'ов. Потом форкну и доведу до ума.
