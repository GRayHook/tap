<?php

  /**
   * Tap
   */
  class Controller_tap extends Controller
  {
    private $_model;
    private $_view;

    function __construct()
    {
      $this->_view = new View();
      $this->_model = new Model_Tap();
    }

    function action_main()
    {
      $data = [
        'projects' => $this->_model->get_data(),
        'view_title' => '"Тык!"',
        'view_styles' => [ 'tap/main' ],
        'favicon' => '/img/favicon.ico',
        'extended_header' => [
          '<meta charset="utf-8">',
          '<meta name="viewport" content="width=device-width, user-scalable=no">'
        ]
      ];
      $this->_view->generate('tap_index.php', 'base_template.php', $data);
    }

    public function action_viewer()
    {
      if (isset($_GET['room_id'])) {
        $roomName = $this->_model->get_roomName($_GET['room_id']);
      } else {
        $roomName = 'Показ слайдов';
      }
      $data = [
        'roomName' => $roomName,
        'pics' => isset($_GET['room_id']) ?
                  $this->_model->get_pics($_GET['room_id'])
                  : $this->_model->get_pics(null),
        'view_title' => "{$roomName} - \"Тык!\"",
        'favicon' => '/img/favicon.ico',
        'view_styles' => [ 'tap/slider', 'tap/dbg' ],
        'view_scripts' => [ 'tap/main', 'libs/touches', 'libs/slider' ],
        'extended_header' => [
          '<meta charset="utf-8">',
          // '<meta name="viewport" content="width=device-width, user-scalable=no">'
        ]
      ];
      $this->_view->generate('tap_viewer.php', 'base_template.php', $data);
    }
    public function action_upload()
    {
      $data = [
        'view_title' => "Загрузка - \"Тык!\"",
        'favicon' => '/img/favicon.ico',
        'view_styles' => [ 'tap/upload' ],
        'view_scripts' => [ 'tap/upload' ],
        'extended_header' => [
          '<meta charset="utf-8">',
          '<meta name="viewport" content="width=device-width, user-scalable=no">'
        ]
      ];
      $this->_view->generate('tap_upload.php', 'base_template.php', $data);
    }
    public function action_uploader()
    {
      $this->_model->upload();
    }
    public function action_start()
    {
      $data = [
        'projects' => $this->_model->get_data(),
        'view_title' => 'Как начать? — "Тык!"',
        'view_styles' => [ 'tap/main' ],
        'favicon' => '/img/favicon.ico',
        'extended_header' => [
          '<meta charset="utf-8">',
          '<meta name="viewport" content="width=device-width, user-scalable=no">'
        ]
      ];
      $this->_view->generate('tap_start.php', 'base_template.php', $data);
    }
  }


?>
