<?php

class Model_Tap extends Model
{
  private $db;

  function __construct()
  {
    include 'connect.php';
    $this->db = new mysqli($host, $user, $pass, $db);
    $this->db->query("SET NAMES 'utf8' COLLATE 'utf8_general_ci'");
    $this->db->query("SET CHARACTER SET 'utf8'");
    if ($this->db->connect_errno) {
        printf("Не удалось подключиться: %s\n", $this->db->connect_error);
        exit();
    };
  }

  public function upload($rootDir = '/var/www/vhosts/smthn/Bloje/')
  {
    ini_set('upload_max_filesize', '32M');

    $uploadDir = $rootDir.'tap-data/';
    $uploadFile = $uploadDir.basename($_FILES['presentashka']['name']);

    $boolka = true;
    while ($boolka) {
      $str4ka = $this::_generateRandTxt();
      $unpackDir = $uploadDir.$str4ka.'/';
      $boolka = !mkdir($unpackDir, 0777, true);
    }

    $zip = new ZipArchive;
    if ($zip->open($_FILES['presentashka']['tmp_name']) === TRUE) {
      $zip->extractTo($unpackDir);
      $zip->close();
      echo $str4ka;
    } else {
      echo "x\n";
      print_r($_FILES);
    }

    $shell_cmd = 'python '.$rootDir.'py-scripts/RenameFiles.py '.$unpackDir;
    exec($shell_cmd);

    if ($_POST["name"]) {
      $name = $_POST["name"];
    } else {
      $name = 'Презентация';
    }
    $query = 'INSERT INTO `presentashki` (`id`, `name`)'.
             ' VALUES ("'.$str4ka.'", "'.$name.'");';
    $this->db->query($query);

  }

  public function get_roomName($room)
  {
    $query = "SELECT `name` FROM `presentashki` WHERE `id` = '{$room}'";
    $result = $this->db->query($query);
    $name = $result->fetch_row()[0];
    return $name;
  }
  public function get_pics($room_id)
  {
    if (null !== $room_id) {
      $dirka = 'tap-data/'.$_GET['room_id'];
    } else {
      $dirka = 'tap-data/presentashka';
    }
    $pics = [];
    foreach (array_diff(scandir($dirka), array('..', '.')) as $pic) {
      array_push($pics, "/{$dirka}/{$pic}");
    }
    return $pics;
  }

  private static function _generateRandTxt($length = 5)
  {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $numChars = strlen($chars);
    $str4ka = '';
    for ($i = 0; $i < $length; $i++) {
      $str4ka .= substr($chars, rand(1, $numChars) - 1, 1);
    }
    return $str4ka;
  }
}
?>
