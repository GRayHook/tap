# coding: utf-8
"""Сервер на WebSockets"""

import struct
import array
import socket
import threading
from hashlib import sha1
from base64 import b64encode

def start_server(port, size, on_open, on_recv):
    """Метод запуска сервера"""
    my_sock = socket.socket()
    my_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_sock.bind(('', port))
    my_sock.listen(1)
    while True:
        conn, addr = my_sock.accept()
        print 'Connected by ', addr
        arguments = (conn, size, on_open, on_recv)
        threading.Thread(target=handle, args=arguments).start()

def handle(conn, size, on_open, on_receive):
    """Обработчик сообщений"""
    data = conn.recv(1024)
    conn.send(create_handshake(data))
    on_open(conn)
    boolka = True
    while boolka:
        try:
            data = conn.recv(size)
            str4ka = unpack_frame(data)['payload']
            if data:
                boolka = on_receive(conn, str4ka)
        except struct.error as excepshn:
            boolka = False
            print 'Словил коллизию ', excepshn, ' ', conn
        except socket.timeout as excepshn:
            boolka = False
            print 'Клиент потерялся ', excepshn, ' ', conn
    # data = conn.recv(1024)
    # conn.send(pack_frame(unpack_frame(data)['payload'], 0x1))
    conn.close()

def send_packed(connection, txt):
    """Cахар для запакованной отправки строк"""
    connection.send(pack_frame(txt, 0x1))

def create_handshake(handshake):
    """Хэндшейк для WebSocket"""
    lines = handshake.splitlines() # Делим построчно
    for line in lines: #Итерируемся по строкам
        parts = line.partition(": ") # Делим по ':'
        if parts[0] == "Sec-WebSocket-Key":
            key = parts[2] # Находим необходимый ключ
    key += "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
    acckey = b64encode((sha1(key)).digest())
    return (
        "HTTP/1.1 101 Switching Protocols\r\n"
        "Upgrade: websocket\r\n"
        "Connection: Upgrade\r\n"
        "Sec-WebSocket-Accept: %s "
        "\r\n\r\n"
        ) % (acckey)

def unpack_frame(data):
    """Функция распаковки фрейма"""
    frame = {}
    byte1, byte2 = struct.unpack_from('!BB', data)
    frame['fin'] = (byte1 >> 7) & 1
    frame['opcode'] = byte1 & 0xf
    masked = (byte2 >> 7) & 1
    frame['masked'] = masked
    mask_offset = 4 if masked else 0
    payload_hint = byte2 & 0x7f
    if payload_hint < 126:
        payload_offset = 2
        payload_length = payload_hint
    elif payload_hint == 126:
        payload_offset = 4
        payload_length = struct.unpack_from('!H', data, 2)[0]
    elif payload_hint == 127:
        payload_offset = 8
        payload_length = struct.unpack_from('!Q', data, 2)[0]
    frame['length'] = payload_length
    payload = array.array('B')
    payload.fromstring(data[payload_offset + mask_offset:])
    if masked:
        mask_bytes = struct.unpack_from('!BBBB', data, payload_offset)
        for i in xrange(len(payload)):
            payload[i] ^= mask_bytes[i % 4]
    frame['payload'] = payload.tostring()
    return frame

def pack_frame(buf, opcode, base64=False):
    """Функция запаковки фреймов"""
    if base64:
        buf = b64encode(buf)
    first_byte = 0x80 | (opcode & 0x0f) # FIN + opcode
    payload_len = len(buf)
    if payload_len <= 125:
        header = struct.pack('>BB', first_byte, payload_len)
    elif payload_len > 125 and payload_len < 65536:
        header = struct.pack('>BBH', first_byte, 126, payload_len)
    elif payload_len >= 65536:
        header = struct.pack('>BBQ', first_byte, 127, payload_len)
    return header+buf

def on_open_test(connection):
    """Тестовый обработчик on_receive"""
    print connection

def on_receive_test(connection, data):
    """Тестовый обработчик on_receive"""
    print connection, data
    return True

if __name__ == '__main__':
    try:
        start_server(29999, 1024, on_open_test, on_receive_test)
    except KeyboardInterrupt:
        exit()
