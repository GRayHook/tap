# coding: utf-8
# pylint: disable=C0103
"""Модуль для *сюрприз* переименовки файлов"""

import os
import sys

def rename(path):
    """Тело"""
    os.chdir(path)
    for filename in os.listdir(path):
        os.rename(filename, renamer(filename))

def renamer(name):
    """Переименование"""
    new_name = name
    if len(name) > 1:
        if name[0:10] == "Слайд":
            otkyda = 10
        if name[0:13] == "Слайд - ":
            otkyda = 13
        else:
            otkyda = 0
        to4ka = name.rindex(".")
        len_of_num = to4ka - otkyda
        qnt_of_zeros = 4 - len_of_num
        new_name = qnt_of_zeros * "0" + name[otkyda:to4ka] + ".jpg"

    return new_name

if __name__ == "__main__":
    # if sys.argv[1]:
    rename(sys.argv[1])
    # else:
    #     rename("/home/grayhook/")
