# coding: utf-8
# pylint: disable=W0603, C0103
"""Модуль для tap"""

import random
import WebSockets

def main():
    """Тело модуля"""
    try:
        WebSockets.start_server(51117, 16, on_open, on_receive)
    except KeyboardInterrupt:
        exit()

rooms = []

def on_open(conn):
    """Обработчик открытия соединения по протоколу WebSockets"""
    print "Я соединился!", conn
    conn.settimeout(7.0)

def on_receive(conn, str4ka):
    """Обработчик входящих сообщений"""
    conn.settimeout(7.0)
    if len(str4ka) > 2:
        act = str4ka[0:3]
        if act != "drs":
            print str4ka, conn
        return ANSWERS.get(act, nthng)(conn, str4ka)

def mkme_host(conn, str4ka):
    """Сделать подключение хостом"""
    if find_room_by_id(str4ka[3:]):
        room = find_room_by_id(str4ka[3:])
        room.host = conn
    else:
        room = Room(conn, str4ka[3:])
    WebSockets.send_packed(conn, room.id)
    return True

def mkme_guest(conn, str4ka):
    """Сделать подключение гостем"""
    room_id = str4ka[3:]
    room = find_room_by_id(room_id)
    room.guests += [conn]
    try:
        WebSockets.send_packed(room.host, "device joined")
    except WebSockets.socket.error as e:
        print e, ' ', conn
        conn.close()
        room.guests.remove([conn])
    WebSockets.send_packed(conn, room.slide)
    return True

def chnge_slide(conn, str4ka):
    """Сменить слайд на всех устройствах"""
    room = find_room(conn)
    room.slide = str4ka
    send_in_room(room.host, room.slide)
    return True

def dscnct_me(conn, str4ka):
    """Отключает устройство"""
    print str4ka, conn
    for i in xrange(len(rooms)):
        for guest in rooms[i].guests:
            if rooms[i].host == conn:
                # WebSockets.send_packed(guest, "disconnected")
                guest.close()
        del rooms[i]
    conn.close()
    return False

def nthng(conn, str4ka):
    """Ничего не делать"""
    print str4ka, conn
    return True

ANSWERS = {
    'hom': mkme_host,
    'gom': mkme_guest,
    'sld': chnge_slide,
    'bby': dscnct_me
}

def find_room(conn):
    """Возвращает комнату, в котором сидит заданное подключение"""
    for room in rooms:
        if room.host == conn:
            return room
        else:
            for guest in room.guests:
                if guest == conn:
                    return room

def find_room_by_id(room_id):
    """Возвращает комнату с указанным id"""
    for i in xrange(len(rooms)):
        if rooms[i].id == room_id:
            return rooms[i]

def send_in_room(host, str4ka):
    """Пакует и отсылает str4ka в комнату с хостом host"""
    for room in rooms:
        if room.host == host:
            for i in xrange(len(room.guests), 0, -1):
                try:
                    WebSockets.send_packed(room.guests[i-1], str4ka)
                except WebSockets.socket.error as e:
                    print e, ' ', room.guests[i-1]
                    room.guests[i-1].close()
                    room.guests.remove(room.guests[i-1])

class Room(object):
    """Комната"""
    def __init__(self, host, room_id):
        global rooms
        self.host = host
        self.guests = []
        self.slide = 'sld0'
        if room_id:
            self.id = room_id
        else:
            self.id = mk_identificator(rooms)
        rooms += [self]
        print 'я родился!'
    def drop_guests(self):
        """Отключить всех гостей"""
        for i in xrange(len(self.guests)):
            self.guests[i].close()
            del self.guests[i]
    def join(self, guest):
        """Добавить гостя"""
        self.guests += guest

def mk_identificator(others):
    """Рандомайзер строки идентификатора"""
    str4ka = random_char()
    boolka = True
    while boolka:
        boolka = False
        str4ka += random_char()
        for elem in others:
            if elem == str4ka:
                boolka = True
    return str4ka

def random_char():
    """Возвращает рандомный символ"""
    return random.choice('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVW'+\
    'XYZ1234567890')

if __name__ == '__main__':
    main()
